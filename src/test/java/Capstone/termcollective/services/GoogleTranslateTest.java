package Capstone.termcollective.services;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import Capstone.termcollective.services.GoogleTranslate;
import Capstone.termcollective.services.ImageService;

public class GoogleTranslateTest {

	@Test
	public void TranslateEnglishToFrench() {
		GoogleTranslate translator = new GoogleTranslate("AIzaSyCd-jLRiuC6j51QyCzel_K3mCJJdcH2sAw");
		String text = translator.translate("How are you. I am fine", "en", "fr");
		assertEquals(text, "Comment vas-tu. je vais bien");
	}

	@Test
	public void TranslateSpanishToEnglish() {
		GoogleTranslate translator = new GoogleTranslate("AIzaSyCd-jLRiuC6j51QyCzel_K3mCJJdcH2sAw");
		String text = translator.translate("Hola", "es", "en");
		assertEquals(text, "Hello");
	}

	@Test
	public void TranslateNonWord() {
		GoogleTranslate translator = new GoogleTranslate("AIzaSyCd-jLRiuC6j51QyCzel_K3mCJJdcH2sAw");
		String text = translator.translate("!j8023", "en", "fr");
		assertEquals(text, "! j8023");
	}
}

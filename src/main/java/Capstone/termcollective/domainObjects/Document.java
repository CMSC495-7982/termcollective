package Capstone.termcollective.domainObjects;

import java.util.HashMap;

public class Document {

	String text;
	
	HashMap<String, String> metadata;

	public Document(String text, HashMap<String, String> metadata) {
		super();
		this.text = text;
		this.metadata = metadata;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public HashMap<String, String> getMetadata() {
		return metadata;
	}

	public void setMetadata(HashMap<String, String> metadata) {
		this.metadata = metadata;
	}
	
	
}

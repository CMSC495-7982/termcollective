package Capstone.termcollective.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.microsoft.ooxml.OOXMLParser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import Capstone.termcollective.domainObjects.Document;

@Service
public class TikaService {

	public TikaService() {

	};

	public Document parseText(File file) throws IOException, SAXException, TikaException {

		HashMap<String, String> metaDataMap = new HashMap<String, String>();

		// detecting the file type
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		FileInputStream inputstream = new FileInputStream(file);

		ParseContext pcontext = new ParseContext();

		// OOXml parser
		OOXMLParser msofficeparser = new OOXMLParser();
		msofficeparser.parse(inputstream, handler, metadata, pcontext);

		// store document contents in string
		String contents = handler.toString();

		String[] metadataNames = metadata.names();

		// add metadata to map
		for (String name : metadataNames) {
			metaDataMap.put(name, metadata.get(name));
		}

		return new Document(contents, metaDataMap);
	}

	public String parseText(String fileName) throws IOException, SAXException, TikaException {

		AutoDetectParser parser = new AutoDetectParser();
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();

		try (InputStream stream = TikaService.class.getResourceAsStream(fileName)) {
			parser.parse(stream, handler, metadata);
			return handler.toString();
		}
	}

	public String parseFile(File file) throws IOException, SAXException, TikaException {
		AutoDetectParser parser = new AutoDetectParser();
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		FileInputStream inputstream = new FileInputStream(file);
		parser.parse(inputstream, handler, metadata);
		return handler.toString();
	}
}
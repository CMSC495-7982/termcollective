package Capstone.termcollective.services;

import java.io.File;
import net.sourceforge.tess4j.*;
import net.sourceforge.tess4j.util.LoadLibs;
import Capstone.termcollective.services.GoogleTranslate;

public class ImageService {
	public static String crackImage(String filePath) {
		File imageFile = new File(filePath);
		ITesseract instance = new Tesseract();
		File tessDataFolder = LoadLibs.extractTessResources("tessdata");
        //Set the tessdata path
        instance.setDatapath(tessDataFolder.getAbsolutePath());
		instance.setLanguage("eng");
		try {
			String result = instance.doOCR(imageFile);
			return result;
		} catch (TesseractException e) {
			System.err.println(e.getMessage());
			return "Error while reading image";
		}
	}
	
	public static String parseAndTranslateImage(String sourceLanguage, String targetLanguage, File imageFile) {
		ITesseract instance = new Tesseract();
		File tessDataFolder = LoadLibs.extractTessResources("tessdata");
        instance.setDatapath(tessDataFolder.getAbsolutePath());
		instance.setLanguage("eng");
		try {
			String result = instance.doOCR(imageFile);
			GoogleTranslate translator = new GoogleTranslate("AIzaSyCd-jLRiuC6j51QyCzel_K3mCJJdcH2sAw");
			result = translator.translate(result, sourceLanguage, targetLanguage);
			return result;
		} catch (TesseractException e) {
			System.err.println(e.getMessage());
			return "Error while reading image";
		}
	}
}
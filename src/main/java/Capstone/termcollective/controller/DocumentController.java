package Capstone.termcollective.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.json.Json;

import org.json.simple.JSONObject;

import org.apache.tika.exception.TikaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import Capstone.termcollective.services.ImageService;
import Capstone.termcollective.services.TikaService;

@RestController
public class DocumentController {

	private TikaService tikaService;

	@Autowired
	public DocumentController(TikaService tikaService) {
		this.tikaService = tikaService;
	}

	@PostMapping
	public String parseTextFile(@RequestParam MultipartFile textFile) throws IOException, SAXException, TikaException {
		File file = convert(textFile);
		String result;
		result = tikaService.parseFile(file);
		System.out.println(result);
		return result;
	}

	@PostMapping
	@RequestMapping("/translate/image")
	public String parseAndTranslateImageFile(@RequestParam String sourceLanguage, @RequestParam String targetLanguage,
			@RequestParam MultipartFile imageFile) throws IOException, SAXException, TikaException {
		File file = convert(imageFile);
		String result;
		result = ImageService.parseAndTranslateImage(sourceLanguage, targetLanguage, file);
		String json = Json.createObjectBuilder().add("result", result).build().toString();
		return json;
	}

	public File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}
}